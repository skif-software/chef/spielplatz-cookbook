# Spielplatz cookbook

This is a Chef cookbook to setup environment for deploying
applications with `docker-compose` utility and for providing access
to the application state with the help of Git and Gitweb.

## Cookbook development

### Prerequisites

The following software should be installed on your machine:

* Chef Development Kit [ https://downloads.chef.io/chefdk ]

* VirtualBox [ https://www.virtualbox.org ]

* Vagrant (if MS Windows or macOS) [ https://www.vagrantup.com ]

### Usage

```
git clone https://gitlab.com/skif-software/chef/spielplatz-cookbook.git

cd spielplatz-cookbook

kitchen converge
```
Browse to http://localhost:8080 and enjoy the latest TAO Community Edition.  
Default user: admin  
Default password: admin

## Advanced usage

## Miscellaneous
