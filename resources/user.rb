property :user_name, String, name_property: true
property :user_home, String, required: true
property :user_authorized_keys, Hash, default: {}

include Spielplatz::Helper

################################################################################
### CREATE APPLICATION USER
################################################################################
#
action :create do
  spielplatz_user      = new_resource.user_name
  spielplatz_user_home = new_resource.user_home
  spielplatz_auth_keys = ""
  new_resource.user_authorized_keys.each {|k, v| spielplatz_auth_keys << v + "\n"}

  # see https://docs.chef.io/ohai.html
  # BUG: reloading only one plugin doesn't update node['etc']['passwd']
  # debug https://github.com/chef/ohai/blob/master/lib/ohai/plugins/passwd.rb
  # and report
  ohai 'reload' do
    action :nothing
  end

  user spielplatz_user do
    comment 'TAO Spielplatz user'
    manage_home true
    home spielplatz_user_home
    shell '/bin/bash'
    action :create
    notifies :reload, 'ohai[reload]', :immediately
  end if validate_username(spielplatz_user)

  group 'docker' do
    members [spielplatz_user]
    append true
    action :manage
  end

  directory ::File.join(spielplatz_user_home, '.ssh') do
    user spielplatz_user
    group spielplatz_user
    mode '0740'
    action :create
  end

  file ::File.join(spielplatz_user_home, '.ssh', 'authorized_keys') do
    content spielplatz_auth_keys
    user spielplatz_user
    group spielplatz_user
    mode '0640'
    action :create
  end

  docker_network spielplatz_user do
    driver 'bridge'
    subnet lazy { user_subnet(user_id(spielplatz_user)) }
    gateway lazy { user_gateway(user_id(spielplatz_user)) }
    action :create
  end
end

################################################################################
### REMOVE APPLICATION USER
################################################################################
#
action :remove do
  spielplatz_user      = new_resource.user_name
  spielplatz_user_home = new_resource.user_home

  user spielplatz_user do
    manage_home true
    action :remove
  end

  directory spielplatz_user_home do
    recursive true
    action :delete
  end

  docker_network spielplatz_user do
    action :delete
  end
end
