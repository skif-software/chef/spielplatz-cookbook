property :proxy_name, String, name_property: true
property :proxy_fqdn, String, required: true
property :proxy_port, String, required: true
property :proxy_template_source, String, default: 'nginx/proxy.erb', required: false
property :proxy_template_cookbook, String, default: 'spielplatz', required: false
property :proxy_ssl, [true, false, String], default: false

################################################################################
### CREATE AND ENABLE NGINX CONFIGURATION / CREATE SSL CERTIFICATES
################################################################################
#
action :enable do
  # This could be used to include methods
  # ::Chef::Resource::User.send(:include, ::Nginx::Cookbook::Helpers)
  # But we want to be safe
  nginx_dir = '/etc/nginx'
  if new_resource.proxy_ssl == true || new_resource.proxy_ssl == 'selfsigned'
    directory ::File.join(nginx_dir, 'ssl') do
      user   'root'
      group  'root'
      mode   '0640'
      action :create
    end
    ssl_dir = ::File.join(nginx_dir, 'ssl', new_resource.proxy_name)
    directory ssl_dir do
      user   'root'
      group  'root'
      mode   '0640'
      action :create
    end
    # To resolve NGINX bootstrap problem with SSL certs not being available
    # create selfsigned certificates
    acme_selfsigned new_resource.proxy_fqdn do
      crt       ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.crt")
      key       ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.key")
      owner     'root'
      group     'root'
      notifies  :restart, "service[nginx]", :immediate
    end
  end

  template ::File.join(nginx_dir, 'conf.d', 'acme.inc') do
    source   'nginx/acme.inc.erb'
    cookbook 'spielplatz'
    group    'root'
    owner    'root'
    action   :create
  end if new_resource.proxy_ssl == true

  nginx_site new_resource.proxy_name do
    template new_resource.proxy_template_source
    cookbook new_resource.proxy_template_cookbook
    if new_resource.proxy_ssl == true || new_resource.proxy_ssl == 'selfsigned'
      variables({
        proxy_name: new_resource.proxy_name,
        proxy_fqdn: new_resource.proxy_fqdn,
        proxy_port: new_resource.proxy_port,
        proxy_ssl:  new_resource.proxy_ssl,
        ssl_crt:    ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.crt"),
        ssl_key:    ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.key"),
        acme_conf:  ::File.join(nginx_dir, 'conf.d', 'acme.inc')
      })
    else
      variables({
        proxy_name: new_resource.proxy_name,
        proxy_fqdn: new_resource.proxy_fqdn,
        proxy_port: new_resource.proxy_port,
        proxy_ssl:  false
      })
    end
    # notify immediately because next step could be real request validation
    notifies :reload, 'service[nginx]', :immediate
    action :enable
  end

  if new_resource.proxy_ssl == true
    include_recipe 'spielplatz::_acme'
    directory node['spielplatz']['acme']['webroot'] do
      user   'root'
      group  'root'
      mode   '0755'
      action :create
    end
    # Set up contact information. Note the mailto: notation
    acme_account_contact = node.read('spielplatz', 'email') ?
                           "mailto:#{node['spielplatz']['email']}" : ''
    fail "Please set attribute default['spielplatz']['email']" if acme_account_contact.empty?
    # Replace selfsigned certificates
    acme_certificate new_resource.proxy_fqdn do
      crt       ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.crt")
      key       ::File.join(ssl_dir, "#{new_resource.proxy_fqdn}.key")
      wwwroot   node['spielplatz']['acme']['webroot']
      contact   [acme_account_contact]
      owner     'root'
      group     'root'
      notifies  :restart, "service[nginx]", :immediate
    end
  end
  # define service for notifications
  service 'nginx' do
    action :nothing
  end
end

################################################################################
### DISABLE NGINX CONFIGURATION
################################################################################
#
action :disable do
  nginx_site new_resource.proxy_name do
    action :disable
  end
end
