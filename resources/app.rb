property :app_name, String, name_property: true
property :app_path, String, required: true
property :app_user, String, required: true
property :app_version, String, default: ''
property :app_tag, String, default: ''
property :app_url,  String, default: ''
property :app_description, String, default: ''
property :app_spielplatz_name,  String, default: 'spielplatz'
property :app_spielplatz_fqdn,  String, default: 'local'
property :app_git_exclude,      Array,   default: []

def git_path
  return "#{app_path}.git"
end

################################################################################
### CREATE APPLICATION STATE
################################################################################
#
action :create do
  directory new_resource.app_path do
    user new_resource.app_user
    group new_resource.app_user
    mode '0755'
    action :create
  end
  # directory git_path do
  #   user new_resource.app_user
  #   group new_resource.app_user
  #   mode '0755'
  #   action :create
  # end
  execute "git-init-separate-git-dir" do
    command %Q[git init --separate-git-dir #{git_path}]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
    not_if { ::File.exists?(git_path) }
  end
  # TODO: https://git-scm.com/docs/git-config#git-config-gitwebdescription
  template ::File.join(git_path, 'README.html') do
    source 'README.html.erb'
    cookbook 'spielplatz'
    user new_resource.app_user
    group new_resource.app_user
    mode '0644'
    action :create
  end
  file ::File.join(git_path, 'description') do
    content new_resource.app_description
    user new_resource.app_user
    group new_resource.app_user
    mode '0644'
    action :create
  end unless new_resource.app_description.empty?
  execute "git-config-gitweb.owner" do
    command %Q[git config gitweb.owner #{new_resource.app_user}@#{new_resource.app_spielplatz_fqdn}]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
  end
  execute "git-config-gitweb.category" do
    command %Q[git config gitweb.category #{new_resource.app_spielplatz_name}]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
  end
end
#
################################################################################
### UPDATE APPLICATION STATE
################################################################################
#
action :update do
  # because url is known only after port number calculation
  url = []
  # access url
  url << "<a href='#{new_resource.app_url}' >#{new_resource.app_url}</a>" unless new_resource.app_url.empty?
  # clone url
  url << "git clone #{new_resource.app_user}@#{new_resource.app_spielplatz_fqdn}:#{git_path} #{new_resource.app_user}_#{new_resource.app_name}"
  # ssh url
  url << "ssh -i ~/.ssh/id_rsa #{new_resource.app_user}@#{new_resource.app_spielplatz_fqdn}"
  file ::File.join(git_path, 'cloneurl') do
    content url.join("\n")
    user new_resource.app_user
    group new_resource.app_user
    mode '0644'
    action :create
  end
  # execute "git-config-gitweb.url" do
  #   command %Q[git config gitweb.url "#{url}"]
  #   user new_resource.app_user
  #   group new_resource.app_user
  #   cwd new_resource.app_path
  #   action :run
  # end unless new_resource.app_url.empty?
  # execute "git-config-gitweb.url-clone" do
  #   command %Q[git config gitweb.url "#{new_resource.app_user}@#{new_resource.app_spielplatz_fqdn}:#{git_path}"]
  #   user new_resource.app_user
  #   group new_resource.app_user
  #   cwd new_resource.app_path
  #   action :run
  # end
  unless new_resource.app_git_exclude.empty?
    # https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefpathspecapathspec
    git_exclude_list = ". -- "
    new_resource.app_git_exclude.each do |exclude|
      git_exclude_list = git_exclude_list + "':!:#{exclude}' "
    end
  else
    git_exclude_list = ""
  end
  execute "git-add" do
    command %Q[git add -f -A --ignore-errors #{git_exclude_list}]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
  end
  commit_message = ENV["#{new_resource.app_name.upcase}_COMMIT_MESSAGE"] ? ENV["#{new_resource.app_name.upcase}_COMMIT_MESSAGE"] : node['commit_message']
  execute "git-commit" do
    command %Q[git -c user.name="CHEF" -c user.email="#{new_resource.app_user}@#{new_resource.app_spielplatz_fqdn}" commit -am "#{commit_message}"]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
    not_if %Q[git status --porcelain | wc -l | grep ^0$] # do not execute if the git status returns nothing to commit
    only_if %Q[git status --porcelain | grep ^M.*$ || git status --porcelain | grep ^A.*$] # execute only if there new files added or modified
  end
  execute "git-tag" do
    command %Q[git tag v#{new_resource.app_version}-#{new_resource.app_tag}]
    user new_resource.app_user
    group new_resource.app_user
    cwd new_resource.app_path
    action :run
    not_if %Q[git tag | grep "v#{new_resource.app_version}-#{new_resource.app_tag}"]
  end unless new_resource.app_version.empty? && new_resource.app_tag.empty?
  # create link for gitweb
  link ::File.join(node['spielplatz']['git'], "#{new_resource.app_user}_#{new_resource.app_name}.git") do
    to git_path
    user new_resource.app_user
    group new_resource.app_user
    mode '0644'
    action :create
  end
end

################################################################################
### DELETE APPLICATION STATE
################################################################################
#
action :delete do
  directory new_resource.app_path do
    recursive true
    action :delete
  end
  directory git_path do
    recursive true
    action :delete
  end
  link ::File.join(node['spielplatz']['git'], "#{new_resource.app_user}_#{new_resource.app_name}.git") do
    action :delete
  end
end
