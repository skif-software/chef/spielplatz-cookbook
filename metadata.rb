name 'spielplatz'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures spielplatz'
long_description 'Installs/Configures spielplatz'
version '1.7.0'
chef_version '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/spielplatz/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/spielplatz'

depends 'docker', '= 4.10.0'
depends 'nginx', '~> 10.0.1'
# versions pinned to hotfix the dependency on Chef latest version
depends 'ark', '~> 5.1.0'
depends 'acme', '~> 4.1.3'
