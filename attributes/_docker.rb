default['spielplatz']['docker']['install'] = false
# in case we are in a standalone environment Docker should be installed
default['spielplatz']['docker']['version'] = '18.06.1'
# otherwise just check that Docker is at least the minimum version required
default['spielplatz']['docker']['minimum_version'] = '18.06.1'
# docker-compose is installed only on Linux x86_64
default['spielplatz']['docker']['compose_install'] = false
default['spielplatz']['docker']['compose_version'] = '1.23.2'
default['spielplatz']['docker']['compose_sha256']  = '4d618e19b91b9a49f36d041446d96a1a0a067c676330a4f25aca6bbd000de7a9'
default['spielplatz']['docker']['compose_path']    = '/usr/local/bin/docker-compose'
default['spielplatz']['docker']['compose_config']  = nil
