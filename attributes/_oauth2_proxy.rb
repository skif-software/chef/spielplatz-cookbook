default['spielplatz']['oauth2_proxy']['enabled']     = false
default['spielplatz']['oauth2_proxy']['install_dir'] = '/usr/local/'
default['spielplatz']['oauth2_proxy']['config_dir']  = '/usr/local/etc/oauth2_proxy'
default['spielplatz']['oauth2_proxy']['version']     = '3.1.0'
oauth2_proxy_version = node['spielplatz']['oauth2_proxy']['version']
default['spielplatz']['oauth2_proxy']['url'] = "https://github.com/pusher/oauth2_proxy/releases/download/" +
                            "v#{oauth2_proxy_version}/oauth2_proxy-v#{oauth2_proxy_version}.linux-amd64.go1.11.tar.gz"
default['spielplatz']['oauth2_proxy']['checksum'] = 'adef7be37522b3a8b2a984402c565498da280c518d4d34ed3698274098d55472'

# TODO: create templates https://github.com/pusher/oauth2_proxy/blob/master/templates.go

default['spielplatz']['oauth2_proxy']['config']['provider'] = nil
default['spielplatz']['oauth2_proxy']['config']['client_id'] = nil
default['spielplatz']['oauth2_proxy']['config']['client_secret'] = nil
# create cookie secret with
# python -c 'import os,base64; print base64.urlsafe_b64encode(os.urandom(16))'
default['spielplatz']['oauth2_proxy']['config']['cookie_secret'] = nil
default['spielplatz']['oauth2_proxy']['config']['email_domains'] = nil
default['spielplatz']['oauth2_proxy']['config']['github_org'] = nil
default['spielplatz']['oauth2_proxy']['config']['redirect_url'] = nil
default['spielplatz']['oauth2_proxy']['config']['cookie_name'] = nil
default['spielplatz']['oauth2_proxy']['config']['cookie_domain'] = nil
default['spielplatz']['oauth2_proxy']['config']['cookie_secure'] = nil
