https://docs.docker.com/engine/swarm/stack-deploy/#test-the-app-with-compose

# Multiple layers
https://docs.docker.com/develop/develop-images/multistage-build/
https://adilsoncarvalho.com/creating-multiple-images-from-a-single-dockerfile-3f69254b6137

# Volumes
https://docs.docker.com/storage/volumes/
https://docs.docker.com/compose/compose-file/#volume-configuration-reference
https://www.linux.com/learn/docker-volumes-and-networks-compose

# UID/GID issue on mounted files
https://jtreminio.com/blog/running-docker-containers-as-current-host-user/

# Ports
https://stackoverflow.com/questions/40801772/what-is-the-difference-between-docker-compose-ports-vs-expose
https://medium.freecodecamp.org/expose-vs-publish-docker-port-commands-explained-simply-434593dbc9a3
https://serversforhackers.com/dockerized-app/flexible-docker-compose



root@default-ubuntu-1604:~/tao# docker run --name mysql5.7-tao --user 1001:1001 -v /root/tao/services/database/mysql/57:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.7

docker-compose pull
docker-compose up -d --no-build

ARG TAG
FROM ubuntu:${TAG:-latest}

If you specify image as well as build, then Compose names the built image with the webapp and optional tag specified in image:
build: ./dir
image: webapp:tag
https://docs.docker.com/compose/compose-file/#build


https://vsupalov.com/docker-arg-env-variable-guide/

# Proxy container for apps
https://mimiz.github.io/2017/05/18/Configure-docker-httpd-image.html

https://stackoverflow.com/questions/34449511/how-to-make-changes-to-httpd-conf-of-apache-running-inside-docker-container-and



Multiple compose files
https://docs.docker.com/compose/extends/
https://docs.docker.com/compose/extends/#multiple-compose-files


https://stackoverflow.com/questions/39517122/what-is-the-best-way-to-get-uid-from-user-within-chef
https://en.wikipedia.org/wiki/User_identifier

usermod -aG docker $USER

/home/dev-username/tao/app
/home/dev-username/tao/app/composer.*
/home/dev-username/tao/app/[tao code]
/home/dev-username/tao/config
/home/dev-username/tao/config/[tao configuration]
/home/dev-username/tao/data
/home/dev-username/tao/data/[tao data]
/home/dev-username/tao/services # directory for storing services persistent data
/home/dev-username/tao/services/postgresql
/home/dev-username/tao/services/postgresql/9.4.19/
/home/dev-username/tao/services/redis/
docker-compose.yml


container storage orchestration engine enabling persistence for cloud native workloads
https://rexray.io/

https://docs.docker.com/engine/reference/commandline/service_create/
https://docs.docker.com/engine/reference/commandline/volume_create/
https://docs.docker.com/storage/volumes/
https://docs.docker.com/storage/storagedriver/
https://docs.docker.com/docker-for-aws/persistent-data-volumes/
https://github.com/rancher/convoy
https://aws.amazon.com/blogs/compute/amazon-ecs-and-docker-volume-drivers-amazon-ebs/
https://docs.docker.com/engine/extend/plugins_volume/


wait until DB is up
https://stackoverflow.com/questions/31269266/can-opscode-chef-perform-wait-until-a-condition-becomes-true
https://supermarket.chef.io/cookbooks/wait
https://stackoverflow.com/questions/31746182/docker-compose-wait-for-container-x-before-starting-y
https://medium.com/@edgar/how-to-wait-for-a-container-to-be-ready-before-starting-another-container-using-docker-compose-92bab2fc1633
https://github.com/docker-library/postgres/issues/146
https://www.reddit.com/r/docker/comments/7x8ugg/dockercomposewait_some_useful_script_to_wait/
https://www.marksayson.com/blog/wait-until-docker-containers-initialized/
https://docs.docker.com/compose/reference/up/
https://docs.docker.com/compose/startup-order/
https://docs.docker.com/compose/compose-file/#depends_on
https://8thlight.com/blog/dariusz-pasciak/2016/10/17/docker-compose-wait-for-dependencies.html
https://blog.newrelic.com/engineering/docker-health-check-instruction/
https://blog.couchbase.com/docker-health-check-keeping-containers-healthy/
https://blog.sixeyed.com/docker-healthchecks-why-not-to-use-curl-or-iwr/

Redis web UI
https://github.com/hollodotme/readis
https://github.com/ErikDubbelboer/phpRedisAdmin

Questions
1) Security policy for speilplatz endpoints
2) Could there be domain and port ?
3) Redirect from port 80 ?


https://staxmanade.com/2016/07/run-multiple-docker-environments--qa--beta--prod--from-the-same-docker-compose-file-/
https://stackoverflow.com/questions/29894579/is-there-a-multi-user-docker-mode-e-g-for-scientific-clusters
https://coderwall.com/p/s_ydlq/using-user-namespaces-on-docker
https://docs.docker.com/engine/security/userns-remap/
https://success.docker.com/article/introduction-to-user-namespaces-in-docker-engine
https://medium.com/@mccode/understanding-how-uid-and-gid-work-in-docker-containers-c37a01d01cf
https://blog.ippon.tech/docker-and-permission-management/


DATABASE UNDER GIT
https://stackoverflow.com/questions/31633817/is-there-a-way-to-actually-save-a-mysql-database-to-github
https://stackoverflow.com/questions/13608706/using-version-control-git-on-a-mysql-database
some experience
https://www.red-gate.com/hub/product-learning/sql-change-automation/moving-from-application-automation-to-true-devops-by-including-the-database
https://blog.codinghorror.com/get-your-database-under-version-control/
https://code.tutsplus.com/articles/how-to-sync-a-local-remote-wordpress-blog-using-version-control--wp-22135
https://www.linode.com/docs/databases/mysql/back-up-your-mysql-databases/
https://www.linode.com/docs/databases/postgresql/how-to-back-up-your-postgresql-database/
https://stackoverflow.com/questions/846659/how-can-i-put-a-database-under-git-version-control
https://enterprisecraftsmanship.com/2015/08/10/database-versioning-best-practices/
tools
https://dbmstools.com/version-control-tools/postgresql
https://github.com/sqitchers/sqitch

https://www.perforce.com/blog/your-git-repository-database-pluggable-backends-libgit2

https://stackoverflow.com/questions/4475457/add-all-files-to-a-commit-except-a-single-file



APACHE OAUTH
https://stackoverflow.com/questions/25996603/oauth-2-0-authentication-in-http-module
https://github.com/zmartzone/mod_auth_openidc
https://openid.net/developers/certified/



some interesting
https://git.osmocom.org/docker-playground/about/



GITWEB configuration
create README.html in app.git directory
http://bastian.rieck.me/blog/posts/2013/gitweb_readme_workflow/
https://stackoverflow.com/questions/8321649/gitweb-how-to-display-markdown-file-in-html-format-automatically-like-github


OAUTH
https://developer.okta.com/blog/2018/08/28/nginx-auth-request
https://github.com/vouch/vouch-proxy

https://stackoverflow.com/a/16283405
https://www.youtube.com/watch?v=8uBcpsIEz2I
https://github.com/cloudfoundry/uaa

https://medium.com/@madumalt/oauth2-proxy-for-single-page-applications-8f01fd5fdd52
https://github.com/bitly/oauth2_proxy
official fork https://github.com/pusher/oauth2_proxy and it has provider for Gitlab
may consider https://github.com/buzzfeed/sso with auth for google

question about difference between vouch and auth-proxy
https://github.com/vouch/vouch-proxy/issues/74

support for Gitlab provider
https://github.com/pomerium/pomerium/issues/20

https://dev.to/themasch/simple-gitlab-static-review-apps-using-nginx--2d9

good video explaining what oauth and openid
https://www.youtube.com/watch?v=GyCL8AJUhww&feature=youtu.be
some theory links
https://github.com/pomerium/awesome-zero-trust



GERRIT
https://review.openstack.org/#/c/636363/
https://www.gerritcodereview.com/config-gitweb.html
https://www.gerritcodereview.com/config-sso.html
https://docs.google.com/presentation/d/1C73UgQdzZDw0gzpaEqIC6SPujZJhqamyqO1XOHjH-uk/edit#slide=id.g4d6c16487b_1_879
https://gerrit.googlesource.com/plugins/github/+/master/README.md
https://github.com/davido/gerrit-oauth-provider
https://github.com/GerritCodeReview/plugins_oauth
save jenkins logs to S3 through logstash plugin and some ideas to implement data analyses:
https://www.youtube.com/watch?v=XfDaeZy0KTk
https://www.promptcloud.com/blog/continuous-integration-setup-gerrit-jenkins
https://wiki.jenkins.io/display/JENKINS/Gerrit+Trigger

GITWEB
https://git-scm.com/docs/git-http-backend
https://apuntesderootblog.wordpress.com/2015/06/01/how-to-run-gitweb-and-git-http-backend-with-nginx-in-fedora/
https://www.howtoforge.com/tutorial/ubuntu-git-server-installation/
https://wiki.archlinux.org/index.php/gitweb
https://gist.github.com/mcxiaoke/055af99e86f8e8d3176e
https://github.com/git/git/blob/master/Documentation/gitweb.txt
http://www.yolinux.com/TUTORIALS/GitWeb-Configuration.html
