#
# Cookbook:: spielplatz
# Recipe:: _oauth2_proxy

binary      = 'oauth2_proxy-linux-amd64'
version     = node['spielplatz']['oauth2_proxy']['version']
install_dir = node['spielplatz']['oauth2_proxy']['install_dir']
config_dir  = node['spielplatz']['oauth2_proxy']['config_dir']
checksum    = node['spielplatz']['oauth2_proxy']['checksum']

config_file = File.join(config_dir, 'oauth2_proxy.cfg')

[install_dir, config_dir].each do |dir|
  directory dir do
    owner 'root'
    group 'root'
    mode   0755
    action :create
  end
end

ark 'oauth2_proxy' do
  url node['spielplatz']['oauth2_proxy']['url']
  version version
  prefix_root install_dir
  has_binaries [binary]
  checksum  checksum
  owner 'root'
  group 'root'
  action :install
end

# TODO: check if systemd is available
# https://docs.chef.io/resource_systemd_unit.html
systemd_unit 'oauth2_proxy.service' do
  content <<-EOU.gsub(/^\s+/, '')
  # Systemd service file for oauth2_proxy daemon
  #
  # Date: Feb 9, 2016
  # Author: Srdjan Grubor <sgnn7@sgnn7.org>

  [Unit]
  Description=oauth2_proxy daemon service
  After=syslog.target network.target

  [Service]
  # www-data group and user need to be created before using these lines
  User=www-data
  Group=www-data

  ExecStart=#{File.join(install_dir,'bin', binary)} -config=#{config_file}
  ExecReload=/bin/kill -HUP $MAINPID

  KillMode=process
  Restart=always

  [Install]
  WantedBy=multi-user.target
  EOU

  action [:create, :enable]
end

template config_file do
  source 'oauth2_proxy/oauth2_proxy.cfg.erb'
  variables lazy { {config: node['spielplatz']['oauth2_proxy']['config']} }
  user 'root'
  group 'root'
  mode '0644'
  action :create
  notifies :restart, 'systemd_unit[oauth2_proxy.service]', :immediately
end

%w(sign_in.html error.html).each do |t|
  template File.join(config_dir, t) do
    source "oauth2_proxy/#{t}.erb"
    user 'root'
    group 'root'
    mode '0644'
    action :create
    notifies :restart, 'systemd_unit[oauth2_proxy.service]', :immediately
  end
end

# ensure the service runs
systemd_unit 'oauth2_proxy.service' do
  action :start
end

# TODO: implement runit service
