#
# Cookbook:: spielplatz
# Recipe:: _gitweb

package 'libcgi-session-perl'

package 'gitweb'

template '/etc/gitweb.conf' do
  source 'gitweb/gitweb.conf.erb'
  variables({git: node['spielplatz']['git']})
  user 'root'
  group 'root'
  mode '0644'
  action :create
end
template '/usr/share/gitweb/static/gitweb.css' do
  source 'gitweb/gitweb.css.erb'
  user 'root'
  group 'root'
  mode '0644'
  action :create
end
directory node['spielplatz']['git'] do
  user 'root'
  group 'root'
  mode '0755'
  action :create
end
