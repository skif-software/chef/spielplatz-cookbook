#
# Cookbook:: spielplatz
# Recipe:: default

# Initialize run_state
node.run_state['spielplatz'] = {}

apt_update 'update' do
  action :update
end

include_recipe 'spielplatz::_gitweb'

service 'apache2' do
  action [:stop, :disable]
end

include_recipe 'spielplatz::_nginx'

include_recipe 'spielplatz::_oauth2_proxy' if node['spielplatz']['oauth2_proxy']['enabled']

include_recipe 'spielplatz::_acme' if node['spielplatz']['acme']['enabled']

# TAO Spielplatz can be installed either on a single machine or in the Docker Swarm

case node['spielplatz']['location']
when 'local'
  # run on local machine
  include_recipe 'spielplatz::_local'
when 'swarm'
  # run on the swarm cluster node
  include_recipe 'spielplatz::_swarm'
end
