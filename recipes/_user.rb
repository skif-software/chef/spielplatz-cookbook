#
# Cookbook:: spielplatz
# Recipe:: _user

spielplatz_user node['spielplatz']['user'] do
  user_home File.join(node['spielplatz']['home'], node['spielplatz']['user'])
  user_authorized_keys node['spielplatz']['authorized']
  action :create
end if node.read('spielplatz', 'user')

if node.read('spielplatz', 'users')
  node['spielplatz']['users'].each do |u|
    spielplatz_user u do
      user_home File.join(node['spielplatz']['home'], u)
      user_authorized_keys node['spielplatz']['authorized']
      action :create
    end
  end
end
