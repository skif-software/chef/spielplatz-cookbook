#
# Cookbook:: spielplatz
# Recipe:: _docker

# Install Docker
if node['spielplatz']['docker']['install'] && node['kernel']['name'] == 'Linux' && node['kernel']['machine'] == 'x86_64'
  docker_installation_package 'default' do
    version node['spielplatz']['docker']['version']
    setup_docker_repo true
    action :create
    package_options %q|--force-yes -o Dpkg::Options::='--force-confold' -o Dpkg::Options::='--force-all'| # if Ubuntu for example
  end
  docker_service 'default' do
    action [:create, :start]
  end
else # check that Docker is required version
  # TODO
end

# Install Docker compose
if node['spielplatz']['docker']['compose_install'] && node['kernel']['name'] == 'Linux' && node['kernel']['machine'] == 'x86_64'
  # Install docker-composer only on Linux x86_64
  version = node['spielplatz']['docker']['compose_version']

  remote_file node['spielplatz']['docker']['compose_path'] do
    source "https://github.com/docker/compose/releases/download/#{version}/docker-compose-Linux-x86_64"
    checksum node['spielplatz']['docker']['compose_sha256']
    user 'root'
    group 'root'
    mode '0755'
    action :create
    not_if "#{node['spielplatz']['docker']['compose_path']} --version | grep #{version}"
  end
else # check that Docker compose is required version
  # TODO
end

# -------------------------------------------------
# This is a quick and dirty way to install utility
# https://github.com/nicolas-van/docker-compose-wait
# package 'python3-pip'
# execute 'pip3 install docker-compose-wait'
# or place them in directory
# remote_file do
#   source 'https://raw.githubusercontent.com/nicolas-van/docker-compose-wait/master/timeparse.py'
#   action :create
# end
# remote_file do
#   source 'https://raw.githubusercontent.com/nicolas-van/docker-compose-wait/master/docker_compose_wait.py'
#   action :create
# end
# and run with python3
