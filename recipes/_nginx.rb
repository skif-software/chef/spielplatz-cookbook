#
# Cookbook:: spielplatz
# Recipe:: _nginx

package 'fcgiwrap'

nginx_install 'default' do
  source 'distro'
  default_site_enabled false
end

spielplatz_proxy 'gitweb' do
  proxy_fqdn node['spielplatz']['fqdn']
  proxy_port node['spielplatz']['port']
  proxy_ssl node['spielplatz']['port'] == '443' ? true : false
  proxy_template_source 'nginx/gitweb.erb'
  action :enable
end

# ensure NGINX is up
service 'nginx' do
  action :start
end
