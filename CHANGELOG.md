# spielplatz CHANGELOG

This file is used to list changes made in each version of the spielplatz cookbook.

# 1.7.1

- Fix git dir config in gitweb
- Fix acme cookbook version
- Fix ark cookbook version

# 1.7.0

- Enable user home

# 1.6.0

- Add support for git exclude list
- Add git commit guard to support exclude list logic
- Create docker_network for spielplatz user
- Fix git commit guards
- Add user_gateway to docker_network

# 1.5.4

- Add remove action on spielplatz user
- Fix delete action on spielplatz app
- Fix remove action on spielplatz user

# 1.5.3

- Fix FQDN and git user/email in app resource
- Update README.md

# 1.5.2

- Fix NGINX default

# 1.5.1

- Fix acme cookbook dependency constraint

# 1.5.0

- Enable SSL support
- Fix guard in app resource
- Fix return code in redirect for proxy
- Fix ssh access url
- Update gitweb template and nginx recipe
- Fix guard in app resource
- Fix to use nginx cookbook latest version

# 1.4.2

- Enable conditional git tagging in app update action
- Remove deprecated recipes
- Cleanup unused files

# 1.4.1

- Edit README template
- Add html files in gitweb config
- Add custom gitweb.css template
- Add ssh url into description

# 1.4.0

- Add git tagging in app update action

# 1.3.0

- Add spielplatz app delete action

# 1.2.0

- Add spielplatz users attribute

# 1.1.0

- Add spielplatz_user resource

# 1.0.0

Initial release.
